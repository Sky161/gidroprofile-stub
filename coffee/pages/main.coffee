###
* Scripts for main page
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$main = $ '.main'
	$modal = $main.find '.modal'
	$modalBody = $modal.find '.module-body'
	$imgsBody = $modalBody.find '.img .elements'
	$imgs = $imgsBody.find 'a img'
	$linksBody = $modalBody.find '.links ul'
	$links = $linksBody.find 'li a'
